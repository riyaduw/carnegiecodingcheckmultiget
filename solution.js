const axios = require('axios');
var fs = require('fs');

argCount = process.argv.length;
let url = '';
let output = './output.txt';
let downloadSize = 4194304;
const chunk = 4;
switch (argCount) {
    case 3:
        url = process.argv[2].toString();
        break;
    case 4:
        url = process.argv[2].toString();
        output = process.argv[3].toString();
        break;
    case 5:
        url = process.argv[2].toString();
        output = process.argv[3].toString();
        downloadSize = process.argv[4];
        break;
    default:
        console.error("Url is a required parameter");
        return;

}

let chunkSize = Math.floor(downloadSize / chunk);
let remainder = downloadSize % chunk;
let begin = 0;
let end = begin + chunkSize - 1;

function getFirstChunk() {
    return axios({
        method: 'get',
        url: url,
        headers: {
            'Range': `bytes=${begin}-${end}`
        }
    }).then((resp) => {
        const file = fs.createWriteStream(output, {start: begin, end: end});
        file.write(resp.data);
    });
}

begin = end + 1;
end = begin + chunkSize - 1;

function getSecondChunk() {
    return axios({
        method: 'get',
        url: url,
        headers: {
            'Range': `bytes=${begin}-${end}`
        }
    }).then((resp) => {
        const file = fs.createWriteStream(output, {start: begin, end: end});
        file.write(resp.data);
    });
}


begin = end + 1;
end = begin + chunkSize - 1;

function getThirdChunk() {
    return axios({
        method: 'get',
        url: url,
        headers: {
            'Range': `bytes=${begin}-${end}`
        }
    }).then((resp) => {
        const file = fs.createWriteStream(output, {start: begin, end: end});
        file.write(resp.data);
    });
}


begin = end + 1;
end = begin + chunkSize + remainder - 1;

function getFourthChunk() {
    return axios({
        method: 'get',
        url: url,
        headers: {
            'Range': `bytes=${begin}-${end}`
        }
    }).then((resp) => {
        const file = fs.createWriteStream(
            output,
            {
                start: begin, end: end
            });
        file.write(resp.data);
    });
}


axios.all([getFirstChunk(), getSecondChunk(), getThirdChunk(), getFourthChunk()]).then(() => {
    console.log("finished download");
}).catch((err) => {
    console.log(err);
});

